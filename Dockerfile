FROM tensorflow/tensorflow:2.1.0-py3
SHELL ["/bin/bash", "-c"]

#RUN apt-get update  && apt-get -y install --no-install-recommends locales && locale-gen en_US.UTF-8
RUN  echo 'Acquire {http::Pipeline-Depth "0";};' >> /etc/apt/apt.conf
RUN DEBIAN_FRONTEND="noninteractive"
RUN apt-get update  && apt-get -y install --no-install-recommends git locales && locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

RUN pip3 install botocore boto3
RUN mkdir /log && git clone https://gitlab.com/jerry-ai-platform/tensorboard.git /log