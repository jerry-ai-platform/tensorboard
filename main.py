import os
import time
import pickle
import argparse
from boto3.session import Session


def storage_helper_dow(sourceDir: str, bucket_name: str):
    aws_access_key_id = 'minio'
    aws_secret_access_key = 'miniostorage'
    
    session = Session(aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)

    s3 = session.resource('s3', endpoint_url='http://10.121.240.233:9000')

    bucket = s3.Bucket(bucket_name)
    for obj in bucket.objects.filter(Prefix = sourceDir):
        if not os.path.exists(os.path.dirname(obj.key)):
            os.makedirs(os.path.dirname(obj.key))
        bucket.download_file(obj.key, obj.key)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Get Train log script')
    parser.add_argument('--bucket_name', help='buckut_name')
    parser.add_argument('--log_dir', help='target dir in the bucket')
    args = parser.parse_args()

    print("Geeting log file from: "+args.bucket_name)
    storage_helper_dow(sourceDir=args.log_dir, bucket_name=args.bucket_name)
